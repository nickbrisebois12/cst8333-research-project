package main

import (
	cd "./cheesedata"
	"reflect"
	"testing"
)

func TestDB(t *testing.T) {
	useDebugLog := true
	logger, err := createLogger(useDebugLog)

	if err != nil {
		t.Errorf("Error creating logger %v", err)
	}

	cheeseData := cd.NewCheeseData()
	testCheese := cd.Cheese{
		"999", "test", "test", "", "Fromages la faim de loup", "NB", "Farmstead", "Fermière", "", "", "24.2", "47", "", "", "Sharp, lactic", "Marquée et lactée", "Uncooked", "Pâte non cuite", "9 Months", "9 mois", "0", "Firm Cheese", "Pâte ferme", "Ewe", "Brebis", "Raw Milk", "Lait cru", "Washed Rind", "Croûte lavée", "2016-02-03",
	}
	// Add test cheese to datahandler and then write to mongodb
	cheeseData.AddRecord(logger, testCheese)
	cheeseData.WriteToDb(logger)

	// Clear in memory cheese and load from mongodb
	cheeseData.ResetData(logger)
	cheeseData.LoadFromDb(logger)

	var writtenCheese cd.Cheese
	for _, c := range cheeseData.Cheeses {
		if c.ID == testCheese.ID {
			writtenCheese = c
		}
	}

	t.Run("DB IO Testing", func(t *testing.T) {
		if !reflect.DeepEqual(testCheese, writtenCheese) {
			t.Errorf("Writing/loading from database failed")
		}
	})
}

/*
 Cheesedata package represents the data layer, handling all cheese data related tasks
 DataHandler.go handles loading and saving of the data structure holding cheese data
 Author: Nick Brisebois
 Date: October 16, 2019
*/

package cheesedata

import (
	"bufio"
	"context"
	"encoding/csv"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io"
	"log"
	"os"
	"reflect"
	"strings"
	"time"
)

const filePath = "./canadianCheeseDirectory.csv"

// CheeseData holds the main data used for the program
type CheeseData struct {
	Cheeses  []Cheese
	RowCount int
}

type SearchQuery struct {
	Column string
	Search string
}

// NewCheeseData creates a new cheese data handler
func NewCheeseData() *CheeseData {
	self := &CheeseData{
		Cheeses:  make([]Cheese, 0),
		RowCount: 0,
	}
	return self
}

// LoadFromCSV loads all cheeses up to the number of rows given
func (self *CheeseData) LoadFromCSV(logger *log.Logger) {
	csvFile, err := os.Open(filePath)
	defer csvFile.Close()

	if err != nil {
		log.Fatal(err)
	}

	reader := csv.NewReader(bufio.NewReader(csvFile))
	self.Cheeses = make([]Cheese, 0)

	counter := 0
	for {
		line, err := reader.Read()
		counter++

		// Skip the column headers
		if counter == 0 {
			continue
		}

		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		self.AddRecord(logger, Cheese{
			ID:                line[ID],
			NameEn:            line[NameEn],
			NameFr:            line[NameFr],
			MfgNameEn:         line[MfgNameEn],
			MfgNameFr:         line[MfgNameFr],
			MfgProvCode:       line[MfgProvCode],
			MfgTypeEn:         line[MfgTypeEn],
			MfgTypeFr:         line[MfgTypeFr],
			WebSiteEn:         line[WebSiteEn],
			WebSiteFr:         line[WebSiteFr],
			FatPercent:        line[FatPercent],
			MoisturePercent:   line[MoisturePercent],
			ParticularitiesEn: line[ParticularitiesEn],
			ParticularitiesFr: line[ParticularitiesFr],
			FlavourEn:         line[FlavourEn],
			FlavourFr:         line[FlavourFr],
			CharacteristicsEn: line[CharacteristicsEn],
			CharacteristicsFr: line[CharacteristicsFr],
			RipeningEn:        line[RipeningEn],
			RipeningFr:        line[RipeningFr],
			Organic:           line[Organic],
			CatTypeEn:         line[CatTypeEn],
			CatTypeFr:         line[CatTypeFr],
			MilkTypeEn:        line[MilkTypeEn],
			MilkTypeFr:        line[MilkTypeFr],
			MilkTmtTypeEn:     line[MilkTmtTypeEn],
			MilkTmtTypeFr:     line[MilkTmtTypeFr],
			RindTypeEn:        line[RindTypeEn],
			RindTypeFr:        line[RindTypeFr],
			LastUpdated:       line[LastUpdated],
		})
	}
}

// AddRecord adds a new cheese to the CheeseData memory
func (self *CheeseData) AddRecord(logger *log.Logger, newCheese Cheese) {
	go logger.Println("Adding new cheese")
	self.Cheeses = append(self.Cheeses, newCheese)
	self.RowCount++
}

// RemoveRecord deletes a cheese record using the given index
func (self *CheeseData) RemoveRecord(logger *log.Logger, index int) {
	go logger.Printf("Removing cheese at index %d", index)
	self.Cheeses = append(self.Cheeses[:index], self.Cheeses[index+1:]...)
	self.RowCount--
}

func (self *CheeseData) ResetData(logger *log.Logger) {
	go logger.Printf("Resetting CheeseData")
	self.Cheeses = make([]Cheese, 0)
	self.RowCount = 0
}

// WriteToFile writes all data stored in memory to a file
func (self *CheeseData) WriteToFile(logger *log.Logger, fileName string) {

	// Check if first character is a / in filename. This would mean the user has entered
	// a full path rather than just a filename. Otherwise, we"ll just drop the file into
	// the current directory
	if string([]rune(fileName))[0] != '/' {
		currentDir, _ := os.Getwd()
		fileName = currentDir + "/" + fileName
	}

	file, err := os.Create(fileName)

	if err != nil {
		logger.Fatalf("Cannot create file: %v", err)
	}

	writer := csv.NewWriter(file)
	defer writer.Flush()

	headers := []string{
		"CheeseId", "CheeseNameEn", "CheeseNameFr", "ManufacturerNameEn", "ManufacturerNameFr", "ManufacturerProvCode", "ManufacturingTypeEn", "ManufacturingTypeFr", "WebSiteEn", "WebSiteFr", "FatContentPercent", "MoisturePercent", "ParticularitiesEn", "ParticularitiesFr", "FlavourEn", "FlavourFr", "CharacteristicsEn", "CharacteristicsFr", "RipeningEn", "RipeningFr", "Organic", "CategoryTypeEn", "CategoryTypeFr", "MilkTypeEn", "MilkTypeFr", "MilkTreatmentTypeEn", "MilkTreatmentTypeFr", "RindTypeEn", "RindTypeFr", "LastUpdateDate",
	}

	err = writer.Write(headers)
	if err != nil {
		logger.Fatalf("Cannot write header row: %v", err)
	}

	for _, c := range self.Cheeses {
		csvRow := []string{
			c.ID,
			c.NameEn,
			c.NameFr,
			c.MfgNameEn,
			c.MfgNameFr,
			c.MfgProvCode,
			c.MfgTypeEn,
			c.MfgTypeFr,
			c.WebSiteEn,
			c.WebSiteFr,
			c.FatPercent,
			c.MoisturePercent,
			c.ParticularitiesEn,
			c.ParticularitiesFr,
			c.FlavourEn,
			c.FlavourFr,
			c.CharacteristicsEn,
			c.CharacteristicsFr,
			c.RipeningEn,
			c.RipeningFr,
			c.Organic,
			c.CatTypeEn,
			c.CatTypeFr,
			c.MilkTypeEn,
			c.MilkTypeFr,
			c.MilkTmtTypeEn,
			c.MilkTmtTypeFr,
			c.RindTypeEn,
			c.RindTypeFr,
			c.LastUpdated,
		}
		err = writer.Write(csvRow)
		if err != nil {
			logger.Fatalf("Cannot write row: %v", err)
		}

	}
}

// connectToDB is a local function used to connect to the mongodb. It returns a client which must be later closed
func (self *CheeseData) connectToDB(logger *log.Logger) *mongo.Client {
	mongoURI := "mongodb://localhost:27017"
	logger.Println("Initializing connection to DB at " + mongoURI)

	clientOptions := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		go logger.Fatal(err)
	}

	return client
}

func (self *CheeseData) LoadFromDb(logger *log.Logger) {
	client := self.connectToDB(logger)

	logger.Println("Creating DB context")
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	err := client.Ping(ctx, nil)

	if err != nil {
		logger.Fatal(err)
		return
	}

	collection := client.Database("cheeseDirectory").Collection("Records")

	cur, err := collection.Find(ctx, bson.D{})

	for cur.Next(ctx) {
		var c Cheese
		err = cur.Decode(&c)

		if err != nil {
			logger.Fatal("Error on decoding document", err)
			return
		}

		self.AddRecord(logger, c)
	}

	err = client.Disconnect(ctx)

	if err != nil {
		logger.Fatal(err)
	}
}

// WriteToDb writes the current cheese data held in memory to a mongodb database
func (self *CheeseData) WriteToDb(logger *log.Logger) {
	client := self.connectToDB(logger)

	// This creates a timeout, so we will disconnect from the DB after 15 seconds if we lost connection
	logger.Println("Creating DB context")
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	// Verify we have a connection to the DB
	err := client.Ping(ctx, nil)

	if err != nil {
		logger.Fatal(err)
		// Why continue if we aren't even connected to the db?
		return
	}

	// Setup our connection to our collection
	collection := client.Database("cheeseDirectory").Collection("Records")

	for _, c := range self.Cheeses {
		var result Cheese

		// This is a binary-json filter to use for searching for a record with the id c.ID
		idFilter := bson.D{
			{
				Key:   "id",
				Value: c.ID,
			},
		}
		err = collection.FindOne(ctx, idFilter).Decode(&result)

		// If the current record already exists, replace it with the updated one
		if result.ID != "" {
			go logger.Println("Record " + c.ID + " already exists. Replacing it with new one")
			collection.ReplaceOne(ctx, idFilter, c)
		} else {
			go logger.Println("Adding record " + c.ID + " to database")
			_, err := collection.InsertOne(ctx, c)
			if err != nil {
				logger.Fatal(err)
			}
		}
	}

	err = client.Disconnect(ctx)

	if err != nil {
		logger.Fatal(err)
	}
}

func (self *CheeseData) LoadSearchResults(logger *log.Logger, searchQuery string) (err error) {
	parsedQueries, err := parseSearchQuery(searchQuery)

	var results []Cheese

	for _, c := range self.Cheeses {
		match := false
		for _, q := range parsedQueries {
			field := getField(&c, q.Column)
			match = field == q.Search
		}

		if match {
			results = append(results, c)
		}
	}

	self.Cheeses = results

	return err
}

func getField(c *Cheese, field string) string {
	r := reflect.ValueOf(c)
	f := reflect.Indirect(r).FieldByName(field)
	return f.String()
}

func parseSearchQuery(query string) (searchQueries []SearchQuery, err error) {
	queries := strings.Split(query, ";")
	var parsedQueries []SearchQuery

	for _, q := range queries {
		parsedQuery := strings.Split(q, ":")

		parsedQueries = append(parsedQueries, SearchQuery{
			Column: parsedQuery[0],
			Search: parsedQuery[1],
		})
	}

	if len(parsedQueries) == 0 {
		err := errors.New("No valid search queries given")
		return nil, err
	}

	return parsedQueries, nil
}

/*
 Cheesedata package represents the data layer, handling all cheese data related tasks

 Cheese.go mostly holds the struct and enum containing the data of one record but also offers
 methods for manipulating data of a single record
 Author: Nick Brisebois
 Date: October 13, 2019
*/

package cheesedata

import (
	rt "../rawterm"
	"log"
	"strings"
)

// WidgetData Holds data that will be passed to widgets
type WidgetData struct {
	ProgramInfo string
	ControlInfo string
	CheeseData  *CheeseData
}

// Cheese holds cheese data imported from CSV
type Cheese struct {
	ID                string
	NameEn            string
	NameFr            string
	MfgNameEn         string
	MfgNameFr         string
	MfgProvCode       string
	MfgTypeEn         string
	MfgTypeFr         string
	WebSiteEn         string
	WebSiteFr         string
	FatPercent        string
	MoisturePercent   string
	ParticularitiesEn string
	ParticularitiesFr string
	FlavourEn         string
	FlavourFr         string
	CharacteristicsEn string
	CharacteristicsFr string
	RipeningEn        string
	RipeningFr        string
	Organic           string
	CatTypeEn         string
	CatTypeFr         string
	MilkTypeEn        string
	MilkTypeFr        string
	MilkTmtTypeEn     string
	MilkTmtTypeFr     string
	RindTypeEn        string
	RindTypeFr        string
	LastUpdated       string
}

// ColumnName is an enum type signifiying the column
type ColumnName int

// Enum for getting column number from the column name
const (
	// ID is the index of the ID in the data
	ID ColumnName = iota // Start at 0, and count upwards
	// NameEn is the index of the english name
	NameEn
	// NameFr is the index of the french name
	NameFr
	// MfgNameEn is the index of the english manufacturer name
	MfgNameEn
	// MfgNameFr is the index of the french manufacturer name
	MfgNameFr
	// MfgProvCode is the index of the manufacturer's provincial code
	MfgProvCode
	// MfgTypeEn is the index of the manufacturing type in english
	MfgTypeEn
	// MfgTypeFr is the index of the manufacturing type in french
	MfgTypeFr
	// WebSiteEn is the index of the link to the english website
	WebSiteEn
	// WebSiteFr is the index of the link to the french website
	WebSiteFr
	// FatPercent is the index to the fat percentage
	FatPercent
	// MoisturePercent is the index of the moisture percentage
	MoisturePercent
	// ParticularitiesEn is the index of the english particularities
	ParticularitiesEn
	// ParticularitiesFr is the index of the french particularities
	ParticularitiesFr
	// FlavourEn is the index of the flavour in english
	FlavourEn
	// FlavourFr is the index of the flavour in french
	FlavourFr
	// CharacteristicsEn is the index of the characheristics in english
	CharacteristicsEn
	// CharacteristicsFr is the index of the characheristics in French
	CharacteristicsFr
	// RipeningEn is the index of the ripening time in english
	RipeningEn
	// Ripening is the index of the ripening time in french
	RipeningFr
	// Organic is the index to if its organic or not
	Organic
	// CatTypeEn is the index to the english category type
	CatTypeEn
	// CatTypeFr is the index to the french category type
	CatTypeFr
	// MilkTypeEn is the index to the milk type in english
	MilkTypeEn
	// MilkTypeFr is the index to the milk type in french
	MilkTypeFr
	// MilkTmtTypeEn is the index to the milk treatment type in english
	MilkTmtTypeEn
	// MilkTmtTypeFr is the index to the milk treatment type in french
	MilkTmtTypeFr
	// RindTypeEn is the index to the rind type in english
	RindTypeEn
	// RindTypeFr is the index to the rind type in french
	RindTypeFr
	// LastUpdated is the index to the last updated value
	LastUpdated
	// Error is for handling errors related to this enum
	Error
)

// EditCheeseValByIndex opens editor for editing a cheese by its index in the Cheese struct
func (c *Cheese) EditCheeseValByIndex(logger *log.Logger, index ColumnName) {
	switch index {
	case NameEn:
		dataVal := &c.NameEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english name value (current "+*dataVal+")")
		c.NameEn = newVal
		logger.Printf("Got new value for Name(en): %s", newVal)
	case NameFr:
		dataVal := &c.NameFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french name value (current "+*dataVal+")")
		c.NameFr = newVal
		logger.Printf("Got new value for Name(fr): %s", newVal)
	case MfgNameEn:
		dataVal := &c.MfgNameEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english manufacturer name value (current "+*dataVal+")")
		c.MfgNameEn = newVal
		logger.Printf("Got new value for manufacturer name(en): %s", newVal)
	case MfgNameFr:
		dataVal := &c.MfgNameFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french manufacturer name value (current "+*dataVal+")")
		c.MfgNameFr = newVal
		logger.Printf("Got new value for manufacturer name(fr): %s", newVal)
	case MfgProvCode:
		dataVal := &c.MfgProvCode
		newVal := rt.GetQuestionResponse(logger, "Enter a new manufacturer provincial code value (current "+*dataVal+")")
		c.MfgProvCode = newVal
		logger.Printf("Got new value for prov code: %s", newVal)
	case MfgTypeEn:
		dataVal := &c.MfgTypeEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english manufacturing type value (current "+*dataVal+")")
		c.MfgTypeEn = newVal
		logger.Printf("Got new value for mfg type (en): %s", newVal)
	case MfgTypeFr:
		dataVal := &c.MfgTypeFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french manufacturing type value (current "+*dataVal+")")
		c.MfgTypeFr = newVal
		logger.Printf("Got new value for mfg type (fr): %s", newVal)
	case WebSiteEn:
		dataVal := &c.WebSiteEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english website url (current "+*dataVal+")")
		c.WebSiteEn = newVal
		logger.Printf("Got new value for website (en): %s", newVal)
	case WebSiteFr:
		dataVal := &c.WebSiteFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french website url (current "+*dataVal+")")
		c.WebSiteFr = newVal
		logger.Printf("Got new value for website (fr): %s", newVal)
	case FatPercent:
		dataVal := &c.FatPercent
		newVal := rt.GetQuestionResponse(logger, "Enter a new fat percentage: (current "+*dataVal+")")
		c.FatPercent = newVal
		logger.Printf("Got a new value for fat percentage: %s", newVal)
	case MoisturePercent:
		dataVal := &c.MoisturePercent
		newVal := rt.GetQuestionResponse(logger, "Enter a new moisture percentage: (current "+*dataVal+")")
		c.MoisturePercent = newVal
		logger.Printf("Got a new value for moisture percentage: %s", newVal)
	case ParticularitiesEn:
		dataVal := &c.ParticularitiesEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english particularities value: (current "+*dataVal+")")
		c.ParticularitiesEn = newVal
		logger.Printf("Got a new value for particularities: %s", newVal)
	case ParticularitiesFr:
		dataVal := &c.ParticularitiesFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french particularities value: (current "+*dataVal+")")
		c.ParticularitiesFr = newVal
		logger.Printf("Got a new value for particularities: %s", newVal)
	case FlavourEn:
		dataVal := &c.FlavourEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english flavour value: (current "+*dataVal+")")
		c.FlavourEn = newVal
		logger.Printf("Got a new value for flavour(en): %s", newVal)
	case FlavourFr:
		dataVal := &c.FlavourFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french flavour value: (current "+*dataVal+")")
		c.FlavourFr = newVal
		logger.Printf("Got a new value for flavour(fr): %s", newVal)
	case CharacteristicsEn:
		dataVal := &c.CharacteristicsEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english characteristics value: (current "+*dataVal+")")
		c.CharacteristicsEn = newVal
		logger.Printf("Got a new value for characteristics (en): %s", newVal)
	case CharacteristicsFr:
		dataVal := &c.CharacteristicsFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french characteristics value: (current "+*dataVal+")")
		c.CharacteristicsFr = newVal
		logger.Printf("Got a new value for characteristics (fr): %s", newVal)
	case RipeningEn:
		dataVal := &c.RipeningEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new english ripening value: (current "+*dataVal+")")
		c.RipeningEn = newVal
		logger.Printf("Got a new value for ripening(en): %s", newVal)
	case RipeningFr:
		dataVal := &c.RipeningFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new french ripening value: (current "+*dataVal+")")
		c.RipeningFr = newVal
		logger.Printf("Got a new value for ripening(fr): %s", newVal)
	case Organic:
		dataVal := &c.Organic
		newVal := rt.GetQuestionResponse(logger, "Enter a new organic value: (current "+*dataVal+")")
		c.Organic = newVal
		logger.Printf("Got a new value for organic: %s", newVal)
	case CatTypeEn:
		dataVal := &c.CatTypeEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new category type (en): (current "+*dataVal+")")
		c.CatTypeEn = newVal
		logger.Printf("Got a new value for cattypeen: %s", newVal)
	case CatTypeFr:
		dataVal := &c.CatTypeFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new category type (fr): (current "+*dataVal+")")
		c.CatTypeFr = newVal
		logger.Printf("Got a new value for cattype(fr): %s", newVal)
	case MilkTypeEn:
		dataVal := &c.MilkTypeEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new milk type (en): (current "+*dataVal+")")
		c.MilkTypeEn = newVal
		logger.Printf("Got a new value for milk type(en): %s", newVal)
	case MilkTypeFr:
		dataVal := &c.MilkTypeFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new milk type (fr): (current "+*dataVal+")")
		c.MilkTypeFr = newVal
		logger.Printf("Got a new value for milk type(fr): %s", newVal)
	case MilkTmtTypeEn:
		dataVal := &c.MilkTmtTypeEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new milk treatment type (en): (current "+*dataVal+")")
		c.MilkTmtTypeEn = newVal
		logger.Printf("Got a new value for milk treatment type (en): %s", newVal)
	case MilkTmtTypeFr:
		dataVal := &c.MilkTmtTypeFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new milk treatment type (fr): (current "+*dataVal+")")
		c.MilkTmtTypeFr = newVal
		logger.Printf("Got a new value for milk treatment type (fr): %s", newVal)
	case RindTypeEn:
		dataVal := &c.RindTypeEn
		newVal := rt.GetQuestionResponse(logger, "Enter a new rind type(en): (current "+*dataVal+")")
		c.RindTypeEn = newVal
		logger.Printf("Got a new value for rind type (en): %s", newVal)
	case RindTypeFr:
		dataVal := &c.RindTypeFr
		newVal := rt.GetQuestionResponse(logger, "Enter a new rind type(fr): (current "+*dataVal+")")
		c.RindTypeFr = newVal
		logger.Printf("Got a new value for rind type (fr): %s", newVal)
	}
}

func GetTableColumns() Cheese {
	columns := Cheese{
		ID:                "ID",
		NameEn:            "NameEn",
		NameFr:            "NameFr",
		MfgNameEn:         "MfgNameEn",
		MfgNameFr:         "MfgNameFr",
		MfgProvCode:       "MfgProvCode",
		MfgTypeEn:         "MfgTypeEn",
		MfgTypeFr:         "MfgTypeFr",
		WebSiteEn:         "WebsiteEn",
		WebSiteFr:         "WebsiteFr",
		FatPercent:        "FatPercent",
		MoisturePercent:   "MoisturePercent",
		ParticularitiesEn: "ParticularitiesEn",
		ParticularitiesFr: "ParticularitiesFr",
		FlavourEn:         "FlavourEn",
		FlavourFr:         "FlavourFr",
		CharacteristicsEn: "CharacteristicsEn",
		CharacteristicsFr: "CharacteristicsFr",
		RipeningEn:        "RipeningEn",
		RipeningFr:        "RipeningFr",
		Organic:           "Organic",
		CatTypeEn:         "CatTypeEn",
		CatTypeFr:         "CatTypeFr",
		MilkTypeEn:        "MilkTypeEn",
		MilkTypeFr:        "MilkTypeFr",
		MilkTmtTypeEn:     "MilkTmtTypeEn",
		MilkTmtTypeFr:     "MilkTmtTypeFr",
		RindTypeEn:        "RindTypeEn",
		RindTypeFr:        "RindTypeFr",
		LastUpdated:       "LastUpdated",
	}

	return columns
}

func ColumnStringToEnum(columnName string) (name ColumnName, err bool) {

	loweredColumnName := strings.ToLower(columnName)

	switch loweredColumnName {
	case "id":
		return ID, false
	case "nameen":
		return NameEn, false
	case "namefr":
		return NameFr, false
	case "mfgnameen":
		return MfgNameEn, false
	case "mfgnamefr":
		return MfgNameFr, false
	case "mfgprovcode":
		return MfgProvCode, false
	case "mfgtypeen":
		return MfgTypeEn, false
	case "mfgtypefr":
		return MfgTypeFr, false
	case "websiteen":
		return WebSiteEn, false
	case "websitefr":
		return WebSiteFr, false
	case "fatpercent":
		return FatPercent, false
	case "moisturepercent":
		return MoisturePercent, false
	case "particularitiesen":
		return ParticularitiesEn, false
	case "particularitiesfr":
		return ParticularitiesFr, false
	case "flavouren":
		return FlavourEn, false
	case "flavourfr":
		return FlavourFr, false
	case "characteristicsen":
		return CharacteristicsEn, false
	case "characteristicsfr":
		return CharacteristicsFr, false
	case "ripeningen":
		return RipeningEn, false
	case "ripeningfr":
		return RipeningFr, false
	case "organic":
		return Organic, false
	case "cattypeen":
		return CatTypeEn, false
	case "cattypefr":
		return CatTypeFr, false
	case "milktypeen":
		return MilkTypeEn, false
	case "milktypefr":
		return MilkTypeFr, false
	case "milktmttypeen":
		return MilkTmtTypeEn, false
	case "milktmttypefr":
		return MilkTmtTypeFr, false
	case "rindtypeen":
		return RindTypeEn, false
	case "rindtypefr":
		return RindTypeFr, false
	case "lastupdated":
		return LastUpdated, false
	default:
		return Error, true
	}
}

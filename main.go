// Package main holds all the code for printing cheese from cheese CSV
// Author: Nick Brisebois 040843956
// Date: October 2nd, 2019
package main

import (
	"flag"
	"fmt"
	ui "github.com/gizak/termui/v3"
	"io/ioutil"
	log "log"
	"os"
	"time"
)

const logFolder = "./logs/"

// Declare global variables
var (
	// Logger handles all logging to file
	logger *log.Logger
	// This bool handles whether or not the program should continue running
	programRunning bool
)

// createLogger creates a logger that can be shared between functions in order to write to the log file
func createLogger(debug bool) (*log.Logger, error) {
	if debug {
		// Create the log folder if it does not exist
		if err := os.MkdirAll(logFolder, 0777); err != nil {
			fmt.Println("error:")
			return nil, fmt.Errorf("Failed to make log directory: %v", err)
		}

		currentTime := time.Now()
		logFile := fmt.Sprintf("CheeseReader.%s.log", currentTime.Format("2006-01-02-15:04:05"))

		// Open the log file to write to
		f, err := os.OpenFile(logFolder+logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

		if err != nil {
			fmt.Println("error:")
			return nil, fmt.Errorf("Failed to open log file: %v", err)
		}

		logger := log.New(f, "CheeseReader", log.LstdFlags)
		return logger, nil
	} else {
		// Disable logging by writing all logs to /dev/null
		logger := log.New(ioutil.Discard, "", 0666)
		return logger, nil
	}
}

// Main function. Loads csv and then prints it
func main() {
	programRunning = true

	var debugOn = flag.Bool("debug", false, "Enable debug logging?")
	flag.Parse()

	// Setup logger
	var err error
	logger, err = createLogger(*debugOn)
	if err != nil {
		logger.Fatalf("failed to initialize logger. panicking: %v", err)
	}

	// Set up the window
	window := initUI()
	defer ui.Close()

	go logger.Println("Beginning event handler")
	eventLoop(window)
}

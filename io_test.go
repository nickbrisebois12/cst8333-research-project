package main

import (
	cd "./cheesedata"
	"bufio"
	"encoding/csv"
	"os"
	"reflect"
	"testing"
)

// TestFileLoad tests loading canadianCheeseDirectory.csv to make sure file is imported correctly
func TestFileLoad(t *testing.T) {
	logger, err := createLogger(true)

	if err != nil {
		t.Errorf("Error creating logger %v", err)
	}

	wanted := cd.Cheese{
		"228", "", "Sieur de Duplessis (Le)", "", "Fromages la faim de loup", "NB", "Farmstead", "Fermière", "", "", "24.2", "47", "", "", "Sharp, lactic", "Marquée et lactée", "Uncooked", "Pâte non cuite", "9 Months", "9 mois", "0", "Firm Cheese", "Pâte ferme", "Ewe", "Brebis", "Raw Milk", "Lait cru", "Washed Rind", "Croûte lavée", "2016-02-03",
	}

	cheeseData := cd.NewCheeseData()
	cheeseData.LoadFromCSV(logger)

	t.Run("File IO Loading", func(t *testing.T) {
		if !reflect.DeepEqual(wanted, cheeseData.Cheeses[0]) {
			t.Errorf("Loaded file is not same as desired file contents.")
		}
	})
}

func TestFileWrite(t *testing.T) {
	logger, err := createLogger(true)
	const testFilePath = "/tmp/writetest.csv"

	cheeseData := cd.NewCheeseData()
	cheeseData.LoadFromCSV(logger)
	cheeseData.WriteToFile(logger, testFilePath)

	testFile, err := os.Open(testFilePath)
	defer testFile.Close()

	reader := csv.NewReader(bufio.NewReader(testFile))

	loadedCheese := []cd.Cheese{}

	// Load first two lines of csv
	for i := 0; i < 2; i++ {
		var line []string
		line, err = reader.Read()

		loadedCheese = append(loadedCheese, cd.Cheese{
			ID:                line[cd.ID],
			NameEn:            line[cd.NameEn],
			NameFr:            line[cd.NameFr],
			MfgNameEn:         line[cd.MfgNameEn],
			MfgNameFr:         line[cd.MfgNameFr],
			MfgProvCode:       line[cd.MfgProvCode],
			MfgTypeEn:         line[cd.MfgTypeEn],
			MfgTypeFr:         line[cd.MfgTypeFr],
			WebSiteEn:         line[cd.WebSiteEn],
			WebSiteFr:         line[cd.WebSiteFr],
			FatPercent:        line[cd.FatPercent],
			MoisturePercent:   line[cd.MoisturePercent],
			ParticularitiesEn: line[cd.ParticularitiesEn],
			ParticularitiesFr: line[cd.ParticularitiesFr],
			FlavourEn:         line[cd.FlavourEn],
			FlavourFr:         line[cd.FlavourFr],
			CharacteristicsEn: line[cd.CharacteristicsEn],
			CharacteristicsFr: line[cd.CharacteristicsFr],
			RipeningEn:        line[cd.RipeningEn],
			RipeningFr:        line[cd.RipeningFr],
			Organic:           line[cd.Organic],
			CatTypeEn:         line[cd.CatTypeEn],
			CatTypeFr:         line[cd.CatTypeFr],
			MilkTypeEn:        line[cd.MilkTypeEn],
			MilkTypeFr:        line[cd.MilkTypeFr],
			MilkTmtTypeEn:     line[cd.MilkTmtTypeEn],
			MilkTmtTypeFr:     line[cd.MilkTmtTypeFr],
			RindTypeEn:        line[cd.RindTypeEn],
			RindTypeFr:        line[cd.RindTypeFr],
			LastUpdated:       line[cd.LastUpdated],
		})
	}

	// We want to make sure the headers were written correctly
	wantedHeaders := cd.Cheese{
		"CheeseId", "CheeseNameEn", "CheeseNameFr", "ManufacturerNameEn", "ManufacturerNameFr", "ManufacturerProvCode", "ManufacturingTypeEn", "ManufacturingTypeFr", "WebSiteEn", "WebSiteFr", "FatContentPercent", "MoisturePercent", "ParticularitiesEn", "ParticularitiesFr", "FlavourEn", "FlavourFr", "CharacteristicsEn", "CharacteristicsFr", "RipeningEn", "RipeningFr", "Organic", "CategoryTypeEn", "CategoryTypeFr", "MilkTypeEn", "MilkTypeFr", "MilkTreatmentTypeEn", "MilkTreatmentTypeFr", "RindTypeEn", "RindTypeFr", "LastUpdateDate",
	}

	// Get first row of data that was attempted to be written to a file
	wantedContent := cheeseData.Cheeses[0]

	// Print any errors we had during file loading or log creation
	if err != nil {
		t.Errorf("%v", err)
	}

	t.Run("File IO Writing", func(t *testing.T) {
		if !reflect.DeepEqual(wantedHeaders, loadedCheese[0]) {
			t.Errorf("Writing to file failed. Writen file is different from expected")
		}
		if !reflect.DeepEqual(wantedContent, loadedCheese[1]) {
			t.Errorf("Writing to file failed. Writen file is different from expected")
		}
	})
}

/*Package main is the main package of the program handling most of the main tasks
controller.go handles all interaction between the presentation layer and data layer
Author: Nick Brisebois
Date: October 4, 2019
*/
package main

import (
	cd "./cheesedata"
	rt "./rawterm"
	w "./widgets"
	"fmt"
	ui "github.com/gizak/termui/v3"
	"os"
	"time"
)

// eventLoop is the main program loop that tracks anything that happens and responds appropriately
func eventLoop(win *Window) {
	programRunning = true
	uiEvents := ui.PollEvents()

	activePane := w.CheesePane

	win.redraw()

	// Loops endlessly until we exit (programRunning gets set to false)
	for {

		e := <-uiEvents

		// Keys input that should do the same no matter what pane is active
		switch e.ID {
		case "<Escape>", "<C-c>":
			programRunning = false
		}

		if e.ID == "<Resize>" {
			win.resize()
		}

		if activePane == w.MenuPane {
			// Key input that should only work when the menu is active
			switch e.ID {
			case "<Down>":
				go logger.Printf("Scrolling menu down")
				win.widgets.Menu.ScrollDown()
			case "<Up>":
				go logger.Printf("Scrolling menu up")
				win.widgets.Menu.ScrollUp()
			case "<Enter>":
				menuHandler(win)
				uiEvents = ui.PollEvents()
			case "<Tab>":
				go logger.Printf("Switched to cheese pane")
				activePane = w.CheesePane
				win.widgets.Menu.SetInactivePane()
				win.widgets.CheeseTable.SetActivePane()
			}
		} else if activePane == w.CheesePane {
			// Key input that should only work when the spreadsheet is active
			switch e.ID {
			case "<Down>":
				go logger.Printf("Scrolling spreadsheet down")
				go win.widgets.CheeseTable.ScrollDown(logger)
			case "<Up>":
				go logger.Printf("Scrolling spreadsheet up")
				go win.widgets.CheeseTable.ScrollUp(logger)
			case "<Tab>":
				go logger.Printf("Switched to menu pane")
				activePane = w.MenuPane
				win.widgets.CheeseTable.SetInactivePane()
				win.widgets.Menu.SetActivePane()
			case "<Enter>", "e":
				go logger.Printf("Editing cheese data")
				editText(win)
				win.widgets.CheeseTable.UpdateTableData(logger)
			case "<Delete>", "d":
				go logger.Printf("Deleting cheese record")
				index := win.widgets.CheeseTable.GetSelectedRow()
				win.widgets.CheeseTable.CheeseData.RemoveRecord(logger, index)
				win.widgets.CheeseTable.UpdateTableData(logger)
			}
		}

		win.redraw()

		if programRunning != true {
			return
		}
	}
}

// menuHandler handles interaction with the main screen menu
func menuHandler(win *Window) {
	selItem := win.widgets.Menu.GetSelectedMenu()
	go logger.Printf("Selected: %d", selItem)

	switch selItem {
	case w.NewEntry:
		logger.Printf("Adding new entry")
		newEntry(win)
	case w.LoadDataFromCSV:
		logger.Printf("Reloading onscreen data with data from CSV")
		win.widgets.CheeseTable.CheeseData.LoadFromCSV(logger)
		win.widgets.CheeseTable.UpdateTableData(logger)
	case w.WriteChanges:
		logger.Printf("Writing changes to file")
		ui.Close()
		currentDir, _ := os.Getwd()
		fileName := rt.GetQuestionResponse(logger, "Enter a path to save to ["+currentDir+"]")
		go win.widgets.CheeseTable.CheeseData.WriteToFile(logger, fileName)
		initUI()
	case w.LoadFromDb:
		logger.Printf("Loading data from mongodb")
		win.widgets.CheeseTable.CheeseData.LoadFromDb(logger)
		win.widgets.CheeseTable.UpdateTableData(logger)
	case w.WriteToDb:
		logger.Printf("Writing changes to DB")
		win.widgets.CheeseTable.CheeseData.WriteToDb(logger)
	case w.SearchDataset:
		logger.Printf("Searching dataset")
		searchDataset(win)
	case w.Quit:
		logger.Printf("Quit selected, Quitting program")
		programRunning = false
	}
}

// newEntry handles adding a new entry to the in memory data
func newEntry(win *Window) {
	// We have to drop to raw terminal as termui has no form of input
	ui.Close()

	newID := rt.GetQuestionResponse(logger, "Enter ID")
	newNameEn := rt.GetQuestionResponse(logger, "Enter name in english")
	newNameFr := rt.GetQuestionResponse(logger, "Enter name in french")
	newMfgNameEn := rt.GetQuestionResponse(logger, "Enter manufacturer name in english")
	newMfgNameFr := rt.GetQuestionResponse(logger, "Enter manufacturer name in french")
	newMfgProvCode := rt.GetQuestionResponse(logger, "Enter manufacturer province code")
	newMfgTypeEn := rt.GetQuestionResponse(logger, "Enter manufacturer type in english")
	newMfgTypeFr := rt.GetQuestionResponse(logger, "Enter manufacturer type in french")
	newWebsiteEn := rt.GetQuestionResponse(logger, "Enter english website url")
	newWebsiteFr := rt.GetQuestionResponse(logger, "Enter french website url")
	newFatPercent := rt.GetQuestionResponse(logger, "Enter fat percentage")
	newMoisturePercent := rt.GetQuestionResponse(logger, "Enter moisture percentage")
	newPartsEn := rt.GetQuestionResponse(logger, "Enter particularities in english")
	newPartsFr := rt.GetQuestionResponse(logger, "Enter particularities in french")
	newFlavEn := rt.GetQuestionResponse(logger, "Enter flavour in english")
	newFlavFr := rt.GetQuestionResponse(logger, "Enter flavour in french")
	newCharEn := rt.GetQuestionResponse(logger, "Enter characteristics in english")
	newCharFr := rt.GetQuestionResponse(logger, "Enter characteristics in french")
	newRipeningEn := rt.GetQuestionResponse(logger, "Enter ripening in english")
	newRipeningFr := rt.GetQuestionResponse(logger, "Enter ripening in french")
	newOrganic := rt.GetQuestionResponse(logger, "Enter organic (1 for true or 0 for false)")
	newCatTypeEn := rt.GetQuestionResponse(logger, "Enter cat type in english")
	newCatTypeFr := rt.GetQuestionResponse(logger, "Enter cat type in french")
	newMilkTypeEn := rt.GetQuestionResponse(logger, "Enter milk type in english")
	newMilkTypeFr := rt.GetQuestionResponse(logger, "Enter milk type in french")
	newMilkTmtTypeEn := rt.GetQuestionResponse(logger, "Enter milk treatment type in english")
	newMilkTmtTypeFr := rt.GetQuestionResponse(logger, "Enter milk treatment type in french")
	newRindTypeEn := rt.GetQuestionResponse(logger, "Enter rind type in english")
	newRindTypeFr := rt.GetQuestionResponse(logger, "Enter rind type in french")
	newLastUpdated := time.Now().String()

	go logger.Println("Attempting to rebuild UI")

	win.widgets.CheeseTable.CheeseData.AddRecord(logger, cd.Cheese{
		ID:                newID,
		NameEn:            newNameEn,
		NameFr:            newNameFr,
		MfgNameEn:         newMfgNameEn,
		MfgNameFr:         newMfgNameFr,
		MfgProvCode:       newMfgProvCode,
		MfgTypeEn:         newMfgTypeEn,
		MfgTypeFr:         newMfgTypeFr,
		WebSiteEn:         newWebsiteEn,
		WebSiteFr:         newWebsiteFr,
		FatPercent:        newFatPercent,
		MoisturePercent:   newMoisturePercent,
		ParticularitiesEn: newPartsEn,
		ParticularitiesFr: newPartsFr,
		FlavourEn:         newFlavEn,
		FlavourFr:         newFlavFr,
		CharacteristicsEn: newCharEn,
		CharacteristicsFr: newCharFr,
		RipeningEn:        newRipeningEn,
		RipeningFr:        newRipeningFr,
		Organic:           newOrganic,
		CatTypeEn:         newCatTypeEn,
		CatTypeFr:         newCatTypeFr,
		MilkTypeEn:        newMilkTypeEn,
		MilkTypeFr:        newMilkTypeFr,
		MilkTmtTypeEn:     newMilkTmtTypeEn,
		MilkTmtTypeFr:     newMilkTmtTypeFr,
		RindTypeEn:        newRindTypeEn,
		RindTypeFr:        newRindTypeFr,
		LastUpdated:       newLastUpdated,
	})

	initUI()

	win.widgets.CheeseTable.UpdateTableData(logger)
}

// editText handles switching to the edit window on a selected row of data
func editText(win *Window) {
	// Get the currently selected row
	selectedInt := win.widgets.CheeseTable.GetSelectedRow()
	logger.Printf("Selected item: %d", selectedInt)

	// List is empty most likely, so we can't edit
	if selectedInt >= len(win.widgets.CheeseTable.CheeseData.Cheeses) {
		return
	}

	selectedCheese := &win.widgets.CheeseTable.CheeseData.Cheeses[selectedInt]
	editPage := initEditPageWidgets(selectedCheese)

	// Clear the current UI grid and render the new edit page grid
	ui.Render(editPage.grid)

	// Start polling for keyboard events for the new page
	uiEvents := ui.PollEvents()
	doneEditing := false
	for {
		e := <-uiEvents

		switch e.ID {
		case "q":
			doneEditing = true
		case "<Down>":
			logger.Printf("Scrolling menu down")
			editPage.widgets.EditTable.ScrollDown()
		case "<Up>":
			logger.Printf("Scrolling menu up")
			editPage.widgets.EditTable.ScrollUp()
		case "<Enter>", "e":
			selectedData := editPage.widgets.EditTable.GetSelectedMenu()
			ui.Close()
			selectedCheese.EditCheeseValByIndex(logger, selectedData)
			editPage.widgets.EditTable.LoadCheese(selectedCheese)
			ui.Init()
			ui.Render(editPage.grid)
		}

		if doneEditing {
			initUI()
			return
		}

		ui.Clear()
		ui.Render(editPage.grid)
	}
}

// searchDataset sets up the UI for searching
func searchDataset(win *Window) {
	// Close the UI to search
	ui.Close()

	fmt.Println("######################################################")
	fmt.Println("\t\tSEARCH DATASET")
	fmt.Println("######################################################")
	fmt.Println("Search queries can be made with the following syntax:")
	fmt.Println("    ColumnName:Value")
	fmt.Println("\nIf multiple Columns are to be searched, you can separate them with a semicolon like so:")
	fmt.Println("    ColumnOne:ValueOne;ColumnTwo:ValueTwo")
	fmt.Println("\nSearchable column names (case insensitive)")
	fmt.Print("    ")
	fmt.Print(cd.GetTableColumns())
	fmt.Println("------------------------------------------------------------------------------------------")

	searchQuery := rt.GetQuestionResponse(logger, "Enter your search query: ")
	err := win.widgets.CheeseTable.CheeseData.LoadSearchResults(logger, searchQuery)

	/* If there was an error, keep trying to get a valid search query */
	for err != nil {
		fmt.Printf("Error: %v", err)
		searchQuery = rt.GetQuestionResponse(logger, "Enter your search query: ")
		err = win.widgets.CheeseTable.CheeseData.LoadSearchResults(logger, searchQuery)
	}

	initUI()
}

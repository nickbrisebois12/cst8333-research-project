/*Package widgets holds all of the widgets that can be drawn to the UI grid
widgets.go is the container for all widgets on the main screen
Author: Nick Brisebois
Date: October 4, 2019
*/
package widgets

import (
	t "../cheesedata"
	"log"
)

// Widgets holds pointers to all required widgets
type Widgets struct {
	*CheeseTable
	*Menu
	ProgInfo  *Info
	CtrlsInfo *Info
}

// NewWidgets returns a struct holding all widgets
func NewWidgets(logger *log.Logger, widgetData *t.WidgetData) *Widgets {

	self := &Widgets{
		CheeseTable: NewCheeseTable(widgetData.CheeseData),
		Menu:        NewMenu(),
		ProgInfo:    NewInfo("Program Info", widgetData.ProgramInfo),
		CtrlsInfo:   NewInfo("Controls", widgetData.ControlInfo),
	}

	// Update the contents of the cheese table
	self.CheeseTable.UpdateTableData(logger)

	return self
}

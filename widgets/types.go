/*Package widgets holds all of the widgets that can be drawn to the UI grid
types.go holds widget enum data that is used when talking to widgets
Author: Nick Brisebois
Date: October 13, 2019
*/
package widgets

// Enum holds menu option numbering
const (
	// NewEntry is menu option that opens menu to write new changes
	NewEntry = iota
	// LoadDataFromCSV is menu option for reloading data in table
	LoadDataFromCSV
	// WriteChanges is menu option for writing changes to csv file
	WriteChanges
	// LoadFromDb is a menu option for loading cheese data from monogdb
	LoadFromDb
	// WriteToDb is a menu option for writing to a mongodb
	WriteToDb
	// SearchDataset lets you search the currently in memory data
	SearchDataset
	// Quit is menu option for quitting program
	Quit
	// MenuSize is the number of menu items
	MenuSize
)

const (
	// CheesePane represents the cheese panel being active
	CheesePane = iota
	// MenuPane represents the menu panel being active
	MenuPane
)

/*Package widgets holds all of the widgets that can be drawn to the UI grid
EditTable.go is the widget that lists all the values in a Cheese row and lets the user
select a value to edit
Author: Nick Brisebois
Date: October 14, 2019
*/
package widgets

import (
	cd "../cheesedata"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// EditTable is a struct holding a list widget
type EditTable struct {
	*widgets.List
	currentItem int
}

// NewEditTable creates and return a new menu widget
func NewEditTable(cheese *cd.Cheese) *EditTable {
	self := &EditTable{
		List: widgets.NewList(),
	}

	self.List.Title = "Edit Table Entry"

	self.LoadCheese(cheese)

	self.List.BorderStyle = ui.NewStyle(ui.ColorBlue)
	self.List.WrapText = false
	self.List.TextStyle = ui.NewStyle(ui.ColorCyan)
	self.List.SetRect(0, 0, 25, 8)

	return self
}

// LoadCheese Loads cheese data into list
func (e *EditTable) LoadCheese(cheese *cd.Cheese) {
	// Set up menu options. Menu numbering is using enum defined in types.go
	e.List.Rows = []string{
		"[Cheese ID](fg:green,mod:bold): " + cheese.ID,
		"[Cheese Name (english)](fg:green,mod:bold): " + cheese.NameEn,
		"[Cheese Name (french)](fg:green,mod:bold): " + cheese.NameFr,
		"[Manufacturer Name (english)](fg:green,mod:bold): " + cheese.MfgNameEn,
		"[Manufacturer Name (french)](fg:green,mod:bold): " + cheese.MfgNameFr,
		"[Manufacturer Province Code](fg:green,mod:bold): " + cheese.MfgProvCode,
		"[Manufacturing Type (english)](fg:green,mod:bold): " + cheese.MfgTypeEn,
		"[Manufacturing Type (french)](fg:green,mod:bold): " + cheese.MfgTypeFr,
		"[English Website](fg:green,mod:bold): " + cheese.WebSiteEn,
		"[French Website](fg:green,mod:bold): " + cheese.WebSiteFr,
		"[Fat Percent](fg:green,mod:bold): " + cheese.FatPercent,
		"[Moisture Percentage](fg:green,mod:bold): " + cheese.MoisturePercent,
		"[Particularities (english)](fg:green,mod:bold): " + cheese.ParticularitiesEn,
		"[Particularities (french)](fg:green,mod:bold): " + cheese.ParticularitiesFr,
		"[Flavour (english)](fg:green,mod:bold): " + cheese.FlavourEn,
		"[Flavour (french)](fg:green,mod:bold): " + cheese.FlavourFr,
		"[Characteristics (english)](fg:green,mod:bold): " + cheese.CharacteristicsEn,
		"[Characteristics (french)](fg:green,mod:bold): " + cheese.CharacteristicsFr,
		"[Ripening (english)](fg:green,mod:bold): " + cheese.RipeningEn,
		"[Ripening (french)](fg:green,mod:bold): " + cheese.RipeningFr,
		"[Organic](fg:green,mod:bold): " + cheese.Organic,
		"[Category Type (english)](fg:green,mod:bold): " + cheese.CatTypeEn,
		"[Category Type (french)](fg:green,mod:bold): " + cheese.CatTypeFr,
		"[Milk Type (english)](fg:green,mod:bold): " + cheese.MilkTypeEn,
		"[Milk Type (french)](fg:green,mod:bold): " + cheese.MilkTypeFr,
		"[Milk Treatment Type (english)](fg:green,mod:bold): " + cheese.MilkTmtTypeEn,
		"[Milk Treatment Type (french)](fg:green,mod:bold): " + cheese.MilkTmtTypeFr,
		"[Rind Type (english)](fg:green,mod:bold): " + cheese.RindTypeEn,
		"[Rind Type (french)](fg:green,mod:bold): " + cheese.RindTypeFr,
		"[Last Updated](fg:green,mod:bold): " + cheese.LastUpdated,
	}
}

// ScrollUp scrolls menu up
func (e *EditTable) ScrollUp() {
	e.List.ScrollUp()
}

// ScrollDown scrolls menu down
func (e *EditTable) ScrollDown() {
	e.List.ScrollDown()
}

// GetSelectedMenu returns the currently selected menu item
func (e *EditTable) GetSelectedMenu() cd.ColumnName {
	return cd.ColumnName(e.List.SelectedRow)
}

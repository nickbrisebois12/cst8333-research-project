/*Package widgets holds all of the widgets that can be drawn to the UI grid
info.go is a widget that simply prints some information in a nice box
Author: Nick Brisebois
Date: October 8, 2019
*/
package widgets

import (
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// Info is a struct holding an information widget
type Info struct {
	*widgets.Paragraph
}

// NewInfo creates and return a new info widget
func NewInfo(title string, text string) *Info {
	self := &Info{
		Paragraph: widgets.NewParagraph(),
	}

	self.Paragraph.Title = title
	self.Paragraph.Text = text

	self.Paragraph.BorderStyle = ui.NewStyle(ui.ColorWhite)

	return self
}

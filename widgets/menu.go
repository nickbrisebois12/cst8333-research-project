/*Package widgets holds all of the widgets that can be drawn to the UI grid
menu.go is a widget that represents a menu that can be navigated using the arrow keys
Author: Nick Brisebois
Date: October 10, 2019
*/
package widgets

import (
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

// Menu is a struct holding a list widget
type Menu struct {
	*widgets.List
	currentItem int
}

// NewMenu creates and return a new menu widget
func NewMenu() *Menu {
	self := &Menu{
		List: widgets.NewList(),
	}

	self.List.Title = "Menu"

	// Set up menu options. Menu numbering is using enum defined in types.go
	self.List.Rows = make([]string, MenuSize)
	self.List.Rows[NewEntry] = "[0] New Entry"
	self.List.Rows[LoadDataFromCSV] = "[1] Load Data From CSV"
	self.List.Rows[WriteChanges] = "[2] Write Changes To File"
	self.List.Rows[LoadFromDb] = "[3] Load Data from MongoDB"
	self.List.Rows[WriteToDb] = "[4] Write To Database"
	self.List.Rows[SearchDataset] = "[5] Search in memory data"
	self.List.Rows[Quit] = "[6] Quit"

	self.List.BorderStyle = ui.NewStyle(ui.ColorBlue)
	self.List.WrapText = true
	self.List.TextStyle = ui.NewStyle(ui.ColorCyan)
	self.List.SetRect(0, 0, 25, 8)

	return self
}

// ScrollUp scrolls menu up
func (self *Menu) ScrollUp() {
	self.List.ScrollUp()
}

// ScrollDown scrolls menu down
func (self *Menu) ScrollDown() {
	self.List.ScrollDown()
}

// GetSelectedMenu returns the currently selected menu item
func (self *Menu) GetSelectedMenu() int {
	return self.List.SelectedRow
}

// SetActivePane sets the styling to an active panel
func (self *Menu) SetActivePane() {
	self.List.BorderStyle = ui.NewStyle(ui.ColorMagenta)
	self.List.Title = "[Active] Menu"
}

// SetInactivePane Sets the styling to an inactive panel
func (self *Menu) SetInactivePane() {
	self.List.BorderStyle = ui.NewStyle(ui.ColorWhite)
	self.List.Title = "Menu"
}

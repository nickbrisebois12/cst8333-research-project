/*Package widgets holds all of the widgets that can be drawn to the UI grid
editwidgets.go is a struct that holds all widgets that are shown on the
edit row page
Author: Nick Brisebois
Date: October 14, 2019
*/
package widgets

import (
	cd "../cheesedata"
	"log"
)

// EditWidgets holds pointers to all required widgets for editing a row of data
type EditWidgets struct {
	*EditTable
	*Info
}

// NewEditWidgets returns a struct holding all widgets for editing a row of data
func NewEditWidgets(logger *log.Logger, cheese *cd.Cheese) *EditWidgets {

	controls := "Made by Nick Brisebois\n" +
		"[Up/Down]: Arrow Keys to choose a piece of data to edit\n" +
		"[Enter/e]: To edit currently selected piece of data\n" +
		"[q]: To go back to main screen"

	self := &EditWidgets{
		EditTable: NewEditTable(cheese),
		Info:      NewInfo("Controls", controls),
	}

	return self
}

/*
 Widgets package holds all of the widgets that can be drawn to the UI grid

 CheeseTable.go is the widget that presents all the data in a table
 Author: Nick Brisebois
 Date: October 3, 2019
*/

package widgets

import (
	t "../cheesedata"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"log"
)

const (
	// How many cheese items should we draw max
	onScreenCount = 50
)

// CheeseTable holds information on the CheeseTable widget
type CheeseTable struct {
	*widgets.Table
	CheeseData  *t.CheeseData
	SelectedRow int
}

// NewCheeseTable returns a new CheeseTable struct which contains a presentable form of the cheese directory
func NewCheeseTable(cheeseData *t.CheeseData) *CheeseTable {
	self := &CheeseTable{
		Table:       widgets.NewTable(),
		SelectedRow: 1,
		CheeseData:  cheeseData,
	}

	self.Title = "[Active] Cheese Data"

	self.Table.TextStyle = ui.NewStyle(ui.ColorWhite)
	self.Table.RowSeparator = false
	self.Table.BorderStyle = ui.NewStyle(ui.ColorMagenta)
	self.Table.FillRow = true

	self.Table.RowStyles[0] = ui.NewStyle(ui.ColorBlack, ui.ColorMagenta, ui.ModifierBold|ui.ModifierUnderline)
	self.Table.RowStyles[self.SelectedRow] = ui.NewStyle(ui.ColorBlack, ui.ColorWhite, ui.ModifierBold)

	self.Table.SetRect(0, 0, 100, 10)
	return self
}

// UpdateTableData updates the cheeses listed
func (self *CheeseTable) UpdateTableData(logger *log.Logger) {
	self.Table.Rows = make([][]string, 0)

	columns := t.GetTableColumns()

	self.Table.Rows = [][]string{
		[]string{
			columns.ID,
			columns.NameEn,
			columns.MfgNameEn,
			columns.MfgProvCode,
			columns.MfgTypeEn,
			columns.WebSiteEn,
			columns.FatPercent,
			columns.MoisturePercent,
			columns.ParticularitiesEn,
			columns.FlavourEn,
			columns.CharacteristicsEn,
			columns.RipeningEn,
			columns.Organic,
			columns.CatTypeEn,
			columns.MilkTypeEn,
			columns.MilkTmtTypeEn,
			columns.RindTypeEn,
			columns.LastUpdated,
		},
	}

	// Table Scrolling (is hard)
	// Shows (onScreenCount) number of items starting from the current selected row.
	for rowNum := self.SelectedRow - 1; rowNum < len(self.CheeseData.Cheeses) && rowNum < self.SelectedRow+onScreenCount; rowNum++ {
		cheeseRow := self.CheeseData.Cheeses[rowNum]
		logger.Print("Showing: " + cheeseRow.ID)
		newRow := []string{
			cheeseRow.ID,
			cheeseRow.NameEn,
			cheeseRow.MfgNameEn,
			cheeseRow.MfgProvCode,
			cheeseRow.MfgTypeEn,
			cheeseRow.WebSiteEn,
			cheeseRow.FatPercent,
			cheeseRow.MoisturePercent,
			cheeseRow.ParticularitiesEn,
			cheeseRow.FlavourEn,
			cheeseRow.CharacteristicsEn,
			cheeseRow.RipeningEn,
			cheeseRow.Organic,
			cheeseRow.CatTypeEn,
			cheeseRow.MilkTypeEn,
			cheeseRow.MilkTmtTypeEn,
			cheeseRow.RindTypeEn,
			cheeseRow.LastUpdated,
		}
		self.Table.Rows = append(self.Table.Rows, newRow)
	}
}

// ScrollUp scrolls table up
func (self *CheeseTable) ScrollUp(logger *log.Logger) {
	if self.SelectedRow-1 != 0 {
		self.SelectedRow--
		self.UpdateTableData(logger)
	}
}

// ScrollDown scrolls table down
func (self *CheeseTable) ScrollDown(logger *log.Logger) {
	if self.SelectedRow != self.CheeseData.RowCount {
		self.SelectedRow++
		self.UpdateTableData(logger)
	}
}

// SetActivePane sets the styling to an active panel
func (self *CheeseTable) SetActivePane() {
	self.Table.BorderStyle = ui.NewStyle(ui.ColorMagenta)
	self.Table.Title = "[Active] Cheese Data"
}

// SetInactivePane Sets the styling to an inactive panel
func (self *CheeseTable) SetInactivePane() {
	self.Table.BorderStyle = ui.NewStyle(ui.ColorWhite)
	self.Table.Title = "Cheese Data"
}

// GetSelectedRow returns the integer of the selected table row
func (self *CheeseTable) GetSelectedRow() int {
	// Index starts at 1 because of column headers so we just minus one to get correct index
	return self.SelectedRow - 1
}

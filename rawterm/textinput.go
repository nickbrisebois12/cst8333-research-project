/*
 RawTerm package handles raw terminal functions such as user input

 textinput.go handles getting responses to a given question from a raw terminal input
 Author: Nick Brisebois
 Date: October 8, 2019
*/

package rawterm

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// GetQuestionResponse handles dropping to a dumb terminal and getting a response
func GetQuestionResponse(logger *log.Logger, question string) string {
	// TermUI has no built in text input so we have to drop to raw input unfortunately
	logger.Printf("Getting text input for question: %s", question)
	fmt.Printf(question + ": ")
	reader := bufio.NewReader(os.Stdin)

	// Get answer from user
	response, _ := reader.ReadString('\n')

	// Remove the new line character
	response = strings.TrimSuffix(response, "\n")

	return response
}

/*Package main is the main package of the program handling most of the main tasks
window.go handles the creation and functionality of the UI by building and modifying widgets in the widgets package
Author: Nick Brisebois
Date: October 4, 2019
*/
package main

import (
	ui "github.com/gizak/termui/v3"
	//	"go.mongodb.org/mongo-driver/mongo/gridfs"
	//"github.com/gizak/termui/v3/widgets"
	cd "./cheesedata"
	w "./widgets"
)

// Window Holds window and grid information
type Window struct {
	grid    *ui.Grid
	widgets *w.Widgets
}

type editWindow struct {
	grid    *ui.Grid
	widgets *w.EditWidgets
}

// newWindow creates a new main screen window
func newWindow() *Window {
	logger.Printf("Initializing widgets")
	widgets := initWidgets()
	logger.Printf("Initialize window grid")
	grid := newGrid(widgets)

	window := &Window{
		widgets: widgets,
		grid:    grid,
	}

	return window
}

// newGrid creates a new grid layout to store main screen widgets
func newGrid(widgets *w.Widgets) *ui.Grid {
	termWidth, termHeight := ui.TerminalDimensions()
	grid := ui.NewGrid()

	grid.SetRect(0, 0, termWidth, termHeight)

	grid.Set(
		ui.NewRow(2.3/3, widgets.CheeseTable),
		ui.NewRow(0.7/3,
			ui.NewCol(2.0/4, widgets.Menu),
			ui.NewCol(1.0/4, widgets.CtrlsInfo),
			ui.NewCol(1.0/4, widgets.ProgInfo),
		),
	)

	return grid
}

// redraw clears the screen and resets the terminal dimensions to render the screen again nicely
func (w *Window) redraw() {
	ui.Render(w.grid)
}

// resize is called whenever the terminal is resized. It gets the new terminal dimensions and redraws to those
func (w *Window) resize() {
	termWidth, termHeight := ui.TerminalDimensions()
	w.grid.SetRect(0, 0, termWidth, termHeight)
	w.redraw()
}

// initUI simply initializes the termui UI and then returns a new main screen window
func initUI() *Window {
	// Create the terminal UI
	if err := ui.Init(); err != nil {
		logger.Fatalf("failed to initialize termui: %v", err)
	}

	return newWindow()
}

/********************************************************
*					MAIN PAGE							*
*********************************************************/

// initWidgets prepares all the main screen widgets and then returns them in a widget struct
func initWidgets() *w.Widgets {
	programInfo := "Made by Nick Brisebois 040843956"
	controlInfo := "[[<Esc>]](mod:bold,fg:red) to quit program\n" +
		"[[e or <Enter>]](mod:bold,fg:red) to edit selected record\n" +
		"[[d or <Delete>]](mod:bold,fg:red) to delete seleceted record\n" +
		"[[<Tab>]](mod:bold,fg:red) to switch between records and menu\n"

	cheeseData := cd.NewCheeseData()

	widgetData := &cd.WidgetData{
		ProgramInfo: programInfo,
		ControlInfo: controlInfo,
		CheeseData:  cheeseData,
	}

	return w.NewWidgets(logger, widgetData)
}

/********************************************************
*					EDIT PAGE							*
*********************************************************/

// initEditPageWidgets prepares all the edit screen widgets and then returns them in an editWindow struct
func initEditPageWidgets(cheese *cd.Cheese) *editWindow {
	termWidth, termHeight := ui.TerminalDimensions()
	grid := ui.NewGrid()

	grid.SetRect(0, 0, termWidth, termHeight)

	widgets := w.NewEditWidgets(logger, cheese)

	grid.Set(
		ui.NewRow(1.0,
			ui.NewCol(2.0/3, widgets.EditTable),
			ui.NewCol(1.0/3, widgets.Info),
		),
	)

	window := &editWindow{
		grid:    grid,
		widgets: widgets,
	}

	return window
}
